import axios from 'axios'

const delay = (sec: number) => {
  return new Promise((resole, reject) => {
    setTimeout(() => resole(sec), sec * 1000)
  })
}

const instance = axios.create({
  baseURL: 'http//localhost:3000'
})
instance.interceptors.response.use(
  async function (res) {
    await delay(1)
    return res
  },
  function (error) {
    return Promise.reject(error)
  }
)
export default instance
