import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import http from '@/services/http'

export const useLoadingStore = defineStore('loading', () => {
  const isLoading = ref(false)
  const doLoad = () => {
    isLoading.value = true
  }
  const finish = () => {
    isLoading.value = false
  }

  return { isLoading, doLoad, finish }
})
