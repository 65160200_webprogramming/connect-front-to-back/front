import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import http from '@/services/http'
import temperature from '@/services/temperature'
import { useLoadingStore } from '@/stores/loading'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()

  const callConvert = async () => {
    // result.value = convert(celsius.value)
    loadingStore.doLoad()
    try {
      result.value = await temperature.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
  }

  return { valid, celsius, result, callConvert }
})
